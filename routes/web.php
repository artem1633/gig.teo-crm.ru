<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::group(['middleware'=>'auth'], function(){
    //profile
    Route::post('updateprofile','Crm\ProfileController@updateprofile');
    Route::post('updatepassword','Crm\ProfileController@updatepassword');
    Route::get('profile','Crm\ProfileController@profile');
    //users
    Route::get('/users', 'Crm\UserController@index')->name('home');
    Route::get('/createuser', 'Crm\UserController@create');
    Route::post('/saveuser','Crm\UserController@save');
    Route::get('/edituser/{id}', 'Crm\UserController@edit');
    Route::post('/updateuser/{id}', 'Crm\UserController@update');
    //references
    Route::get('/cities', 'Crm\ReferenceController@cities');
    Route::get('/createcity', 'Crm\ReferenceController@createCity');
    Route::post('/savecity', 'Crm\ReferenceController@saveCity');

    Route::get('/places', 'Crm\ReferenceController@places');
    Route::get('/createplace', 'Crm\ReferenceController@createPlace');
    Route::post('/saveplace', 'Crm\ReferenceController@savePlace');

    Route::get('/genres', 'Crm\ReferenceController@genres');
    Route::get('/creatgenre', 'Crm\ReferenceController@createGenre');
    Route::post('/savegenre', 'Crm\ReferenceController@saveGenre');

    Route::get('/organizers', 'Crm\ReferenceController@organizers');
    Route::get('/createorganizer', 'Crm\ReferenceController@createOrganizer');
    Route::post('/saveorganizer', 'Crm\ReferenceController@saveOrganizer');

    //events
    Route::get('/', 'Crm\EventController@index')->name('home');
    Route::get('/createevent', 'Crm\EventController@create');
    Route::get('/ajax/getplatforms', 'Crm\AjaxController@getPlatforms');
    Route::post('/saveevent', 'Crm\EventController@save');
    Route::get('/ajax/getevents/{filter?}', 'Crm\EventController@getEvents');
    Route::get('/event/{id}', 'Crm\EventController@show');
    Route::get('/event/{id}/costs', 'Crm\EventController@costs');
    Route::get('/event/{id}/sales', 'Crm\EventController@eventSale');
    Route::post('/event/{id}/sales', 'Crm\EventController@insertEventSale');
    Route::get('/createexpenditureitem/{id}', 'Crm\EventController@createexpenditureitem');
    Route::post('/saveexpenditureitem/{id}', 'Crm\EventController@saveExpenditureitem');
    Route::get('/editexpenditureitem/{id}', 'Crm\EventController@editExpenditureitem');
    Route::post('/updateexpenditureitem/{id}', 'Crm\EventController@updateExpenditureitem');
    Route::get('/deleteexpenditureitem/{id}','Crm\EventController@deleteExpenditureitem');

    Route::get('/sales', 'Crm\EventController@sales');
    Route::post('/updatesales', 'Crm\EventController@updateSales');
});



