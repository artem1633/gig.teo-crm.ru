<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        DB::table('ccategories')->insert([
            [
                'name' => 'Базовые расходы',
                'slug' => 'base',
            ],
            [
                'name' => 'Технические расходы',
                'slug' => 'technical',
            ],
            [
                'name' => 'Бытовые расходы',
                'slug' => 'household',
            ],
            [
                'name' => 'Логистические расходы',
                'slug' => 'logistic',
            ],
            [
                'name' => 'Расходы на персонал',
                'slug' => 'staff',
            ],
            [
                'name' => 'Административные расходы',
                'slug' => 'administrative',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccategories');
    }
}
