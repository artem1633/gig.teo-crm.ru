<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenditureitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenditureitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ccategory_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->string('name');
            $table->integer('summ_cash')->default(0)->nullable();
            $table->integer('summ_nocash')->default(0)->nullable();
            $table->integer('paid_cash')->default(0)->nullable();
            $table->integer('paid_nocash')->default(0)->nullable();
            $table->integer('surplus_cash')->default(0);
            $table->integer('surplus_nocash')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenditureitems');
    }
}
