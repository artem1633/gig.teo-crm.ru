<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'name' => 'Администратор',
            'alias' => 'Admin',
            'description' => 'Самый главный всей системы',
            'created_at' => DB::raw('now()')
        ], [
            'name' => 'Руководитель',
            'alias' => 'Leader',
            'description' => 'Leader',
            'created_at' => DB::raw('now()')
        ], [
            'name' => 'Концертный администратор',
            'alias' => 'Manager',
            'description' => 'Manager',
            'created_at' => DB::raw('now()')
        ], [
            'name' => 'Отдел продаж',
            'alias' => 'Salesman',
            'description' => 'Salesman',
            'created_at' => DB::raw('now()')
        ], [
            'name' => 'Бухгалтерия',
            'alias' => 'Accountant',
            'description' => 'Accountant',
            'created_at' => DB::raw('now()')
        ]]);
    }
}
