<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    // public function hasRole($name, $requireAll = false)
    // {
    //     if (is_array($name)) {
    //         foreach ($name as $roleName) {
    //             $hasRole = $this->hasRole($roleName);

    //             if ($hasRole && !$requireAll) {
    //                 return true;
    //             } elseif (!$hasRole && $requireAll) {
    //                 return false;
    //             }
    //         }

    //         // If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
    //         // If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
    //         // Return the value of $requireAll;
    //         return $requireAll;
    //     } else {
    //         foreach ($this->roles() as $role) {
    //             if ($role->alias == $name) {
    //                 return true;
    //             }
    //         }
    //     }

    //     return false;
    // }

    /**
    * @param string|array $roles
    */
    public function authorizeRoles($roles)
    {
      if (is_array($roles)) {
          return $this->hasAnyRole($roles) || 
            abort(401, 'This action is unauthorized.');
      }
      return $this->hasRole($roles) || 
            abort(401, 'This action is unauthorized.');
    }
    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('alias', $roles)->first();
    }
    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('alias', $role)->first();
    }

    public function isAdmin()
    {
        return $this->hasRole('Admin');
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
