<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'city_id',
        'platform_id',
        'genre_id',
        'organizer_id',
        'user_id',
        'name',
        'happened_at',
        'tickets_count',
        'tucket_price',
        'summ',
        'is_our',
        'is_new',
    ];
    
    protected $appends = ['not_seen'];


    protected $dates = ['created_at', 'updated_at', ];

    //количество наших мероприятий
    public static function ourEventsCount()
    {
        return Event::where('is_our', 1)
            ->count();
    }

    //получение model событий с параметрами фильтра
    public static function eventsWithFilter($filter)
    {
        if($filter == 'all'){
           return Event::all();
        }
        return Event::where('is_our', $filter)->get();
    }

    //количество других событий
    public static function clientEventsCount()
    {
        return Event::where('is_our', 0)
            ->count();
    }

    //возвращает не увиденный элемент
    public function getNotSeenAttribute()
    {
        $u_id = \Auth::user()->id;
        $unreaded_users = explode('|', $this['is_new']);
        if(!in_array($u_id, $unreaded_users) && ($this->attributes['happened_at'] >= \Carbon\Carbon::now())){
            return $this->attributes['not_seen'] = 'new-event';
        }else{
            return $this->attributes['not_seen'] = 'old-event';
        }        
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    public function organizer()
    {
        return $this->belongsTo(Organizer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function expenditureitems()
    {
        return $this->hasMany(Expenditureitem::class);
    }

    public function sales()
    {
        return $this->hasMany(EventSale::class);
    }
}
