<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;
use Hash;

class ProfileController extends Controller
{
    public function profile()
    {
        return view('crm.profile',['user'=>Auth::User(),'lmenu'=>'profile']);
    }

    public function updateprofile(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nick'=>'required',
            'email'=>'required|unique:users,email,'.Auth::User()->id,
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::User();
        $user->email = $request->email;
        $user->nick = $request->nick;
        $user->save();
        return redirect()->to('/profile')->with('success',"Профиль обновлен.");
    }

    public function updatepassword(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'password'=>'required',

        ]);
        if($validator->fails()){
            return redirect('/admin/profile')
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::User();
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->to('/profile')->with('success',"Пароль обновлен.");
    }
}
