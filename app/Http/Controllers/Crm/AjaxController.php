<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;

class AjaxController extends Controller
{
    public function getPlatforms(Request $request)
    {
        $id = $request->city;
        $city = City::findOrFail($id);
        $platforms = $city->platforms->toJson();
        return $platforms;
    }
}
