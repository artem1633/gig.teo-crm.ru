<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\Platform;
use App\Genre;
use App\Organizer;
use Illuminate\Support\Facades\Validator;

class ReferenceController extends Controller
{
    public function cities()
    {
        return view('crm.cities.index', ['cities'=>City::all(), 'lmenu'=>'references']);
    }

    public function createCity()
    {
        return view('crm.cities.create', ['lmenu' => 'references']);
    }

    public function saveCity(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'city'=>'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $city = City::create([
            'city' => $request->city,
        ]);
        return redirect('/cities')->with('succes','Город добавлен');
    }

    public function places()
    {
        return view('crm.platforms.index', ['platforms'=>Platform::all(), 'lmenu'=>'references']);
    }

    public function createPlace()
    {
        return view('crm.platforms.create', ['cities' => City::all(), 'lmenu' => 'references']);
    }

    public function savePlace(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'platform'=>'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $platform = Platform::create([
            'platform' => $request->platform,
            'city_id' => $request->city_id,
        ]);
        return redirect('/places')->with('success','Площадка добавлена');
    }

    public function genres()
    {
        return view('crm.genres.index', ['genres' => Genre::all(), 'lmenu'=>'references']);
    }

    public function createGenre()
    {
        return view('crm.genres.create',['lmenu'=>'references']);
    }

    public function saveGenre(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'genre'=>'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $genre = Genre::create([
            'genre' => $request->genre,
        ]);
        return redirect('/genres')->with('succes','Жанр добавлен');
    }

    public function organizers()
    {
        return view('crm.organizers.index', ['organizers' => Organizer::all(), 'lmenu'=>'references']);
    }

    public function createOrganizer()
    {
        return view('crm.organizers.create',['lmenu'=>'references']);
    }

    public function saveOrganizer(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'organizer'=>'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $organizer = Organizer::create([
            'organizer' => $request->organizer,
        ]);
        return redirect('/organizers')->with('succes','Организатор добавлен');
    }

}
