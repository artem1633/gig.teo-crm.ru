<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        return view('crm.users.index',['users'=>User::all(), 'lmenu'=>'users']);
    }

    public function create()
    {
        return view('crm.users.create', ['roles'=>Role::all(), 'lmenu'=>'users']);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|unique:users,email',
            'password'=>'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        foreach($request->role_id as $rid){
            $user->roles()->attach($rid);
        }
        return redirect('/users')->with('success','Пользователь создан');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $roles_id = [];
        foreach($user->roles as $role){
            $roles_id[] = $role->id;
        }
        return view('crm.users.edit', ['user'=>$user, 'lmenu'=>'users', 'roles'=> $roles,'roles_id'=>$roles_id]);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|unique:users,email,'.$id,
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password){
            $user->password = bcrypt($request->password);
        }
        $user->save();
        $user->roles()->detach();
        foreach($request->role_id as $rid){
            $user->roles()->attach($rid);
        }
        return redirect('/users')->with('success','Данные пользователя обновлены');
    }
}
