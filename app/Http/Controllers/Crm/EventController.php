<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\City;
use App\Platform;
use App\Organizer;
use App\Genre;
use Illuminate\Support\Facades\Validator;
use App\Ccategory;
use App\Expenditureitem;

class EventController extends Controller
{
    public function index()
    {
        $event = Event::all(); 
        return view('crm.events.index', ['events' => $event, 'lmenu'=>'events'] );
    }

    public function create()
    {
        $cities = City::all();
        $citiesLastInsertedId = $cities->last()->id;
        $platformLastInsertedId = Platform::all()->last()->id;
        return view('crm.events.create',[
            'lmenu'=> "events",
            'genres' => Genre::all(),
            'organizers' =>Organizer::all(),
            'cities' => $cities,
            'citiesLastInsertedId' => $citiesLastInsertedId,
            'platformLastInsertedId' => $platformLastInsertedId,
        ]);
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'platform_id' => 'required',
            'city' => 'unique:cities',
            'tickets_count' => 'required',
            'data_time' => 'required',
            'ticket_price' => 'required',
            'summ' => 'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $event = Event::create([
            'city_id' => $request->city_id,
            'platform_id' => $request->platform_id,
            'genre_id' => $request->genre_id,
            'organizer_id' => $request->organizer_id,
            'name' => $request->name,
            'happened_at' => $request->data_time,
            'tickets_count' => $request->tickets_count,
            'tucket_price' => $request->ticket_price,
            'summ' => $request->summ,
            'is_our' => $request->is_our,


        ]);
        if($request->city){
            $city = City::create([
                'city' => $request->city,
            ]);
        }
        if($request->platform){
            $platform = Platform::create([
                'platform' => $request->platform,
                'city_id' =>  $request->city_id,      
            ]);
        }
       
        return redirect('/');
    }

    public function getEvents(Request $request, $filter = 'all')
    {
        $events = Event::eventsWithFilter($filter);

        $json = [];
        foreach($events as $event){
            $json[] = [
                'id' => $event->id,
                'title' => $event->name,
                'start' => $event->happened_at,
                'end' => $event->happened_at,
                'allDay' => false,
                'itemStatus' => $event->not_seen,
                'eventOrganisation' => $event->is_our ? 'Наша компания' : 'Конкурент',
                'city' => $event->city['city'],
                'platform' => $event->platform['platform'],
                'genre' => $event->genre['genre'],
                'totalSoldPrice' => $event->sales->sum('price'),
                'totalSoldTickets' => $event->sales->sum('qty'),
                'remainderOfTickets' => $event->tickets_count - $event->sales->sum('qty'),
                'remainderOfPrice' => $event->summ - $event->sales->sum('price'),
                'sales' => $event->sales
            ];
        }
        return $json;
    }

    public function show($id)
    {
        $event = Event::findOrFail($id);

        //удаление нового флага с концерта, если пользователь видит его
        $u_id = \Auth::user()->id;
        $unreaded_users = explode('|', $event['is_new']);
        if(!in_array($u_id, $unreaded_users)){
            $event->is_new .= $u_id.'|';
            $event->save();
        }

        return view('crm.events.show', ['event'=>$event, 'lmenu' =>"events"]);
    }

    public function eventSale($id)
    {
        $event = Event::findOrFail($id);
        return view('crm.events.eventsales',['event'=> $event, 'lmenu'=>'events']);
    }

    public function insertEventSale(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'price'=>'required',
            'qty' => 'required',
            'sale_at' => 'required'
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $eventSale = \App\EventSale::create(['event_id' => $id, 'price' => $request->price, 'qty' => $request->qty, 'sale_at' => $request->sale_at]);
        return back()->with('success','Продажа успешно добавлена');
    }

    public function sales()
    {
        $events = Event::whereDate('happened_at','>', date('Y-m-d'))->get();
        return view('crm.events.sales',['events'=> $events, 'lmenu'=>'events']);
    }

    public function updateSales(Request $request)
    {
        $sale_count = $request->sale_count;
        $sale_summ = $request->sale_summ;
        //dd($sale_summ);
        foreach($sale_count as $key=>$count){
            $event = Event::find($key);
            $event->saled_tickets = $count;
            $event->sale_summ = $sale_summ[$key];
            $event->save();
        }
        return redirect('/sales')->with('success','Информация обновлена');
    }

    public function costs($id)
    {
        $event = Event::findOrFail($id);
        $ccategories= Ccategory::with(['expenditureitems' => function($query) use ($id){
            $query->where('event_id',$id);
        }])->get();
        //dd($ccategories->expenditureitems->sum('summ_cash'));
        //dd($ccategories);
        $expendureitems = Expenditureitem::where('event_id',$id)->get();
        return view('crm.events.costs',['event'=>$event,'lmenu'=>'events','ccategories'=>$ccategories]);
    }

    public function createexpenditureitem(Request $request, $id)
    {
        $request->user()->authorizeRoles(['Admin', 'Manager']);
        $event = Event::findOrFail($id);
        $ccategories = Ccategory::all();
        return view('crm.events.createexpenditureitem',['event'=>$event,'lmenu'=>'events','ccategories'=>$ccategories]);
        //dd($event);
    }

    public function saveExpenditureitem(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $validator = Validator::make($request->all(),[
            'name'=>'required',
        ]);
        if($validator->fails()){
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $expenditureitem = Expenditureitem::create([
            'event_id' => $id,
            'ccategory_id' => $request->ccategory_id,
            'name' => $request->name,
            'summ_cash' => $request->summ_cash,
            'summ_nocash'=> $request->summ_nocash,
            'paid_cash' => $request->paid_cash,
            'paid_nocash'=>$request->paid_nocash,
        ]);
        return redirect('/event/'.$id.'/costs');
    }

    public function editExpenditureitem($id)
    {
        $expenditureitem = Expenditureitem::findOrFail($id);
        $ccategories = Ccategory::all();
        return view('crm.events.editexpenditureitem',['expenditureitem'=>$expenditureitem,'lmenu'=>'events','ccategories'=>$ccategories]);
    }

    public function updateExpenditureitem(Request $request, $id)
    {
        $expenditureitem = Expenditureitem::findOrFail($id);
        $expenditureitem->name = $request->name;
        $expenditureitem->ccategory_id = $request->ccategory_id;
        $expenditureitem->name = $request->name;
        $expenditureitem->summ_cash = $request->summ_cash;
        $expenditureitem->summ_nocash = $request->summ_nocash;
        $expenditureitem->paid_cash = $request->paid_cash;
        $expenditureitem->paid_nocash = $request->paid_nocash;
        $expenditureitem->save();
        return redirect('/event/'.$expenditureitem->event->id.'/costs');
    }

    public function deleteExpenditureitem($id)
    {
        $expenditureitem = Expenditureitem::findOrFail($id);
        $expenditureitem->delete();
        return redirect('/event/'.$expenditureitem->event->id.'/costs');
    }
}
