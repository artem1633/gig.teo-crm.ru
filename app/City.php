<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['city'];

    public function platforms()
    {
        return $this->hasMany(Platform::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
