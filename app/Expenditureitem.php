<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenditureitem extends Model
{
    protected  $fillable =[
        'event_id',
        'ccategory_id',
        'name',
        'summ_cash',
        'summ_nocash',
        'paid_cash',
        'paid_nocash',
    ];

    public function ccategory()
    {
        return $this->belongsTo(Ccategory::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
