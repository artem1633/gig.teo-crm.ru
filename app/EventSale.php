<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSale extends Model
{
	protected $fillable = [
        'event_id',
        'price',
        'qty',
        'sale_at'
    ];

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
