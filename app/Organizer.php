<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    protected $fillable = ['organizer'];

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
