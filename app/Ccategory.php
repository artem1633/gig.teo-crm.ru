<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ccategory extends Model
{
    public function expenditureitems()
    {
        return $this->hasMany(Expenditureitem::class);
    }
}
