<ul>
    <li><a href="/" class="nav-header"><i class="fa fa-fw fa-file"></i> Концерты</a></li>
    <li><a href="/sales" class="nav-header"><i class="fa fa-fw fa-file"></i> Продажи</a></li>
    <li><a href="/users" class="nav-header"><i class="fa fa-fw fa-file"></i> Пользователи</a></li>

    <li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-cog"></i> Справочники<i class="fa fa-collapse"></i></a></li>
    <li><ul class="legal-menu nav nav-list collapse @if($lmenu == 'references') in @endif">
            <li><a href="/cities" class="nav-header"><i class="fa fa-fw fa-file"></i> Города</a></li>
            <li><a href="/places" class="nav-header"><i class="fa fa-fw fa-file"></i> Площадки</a></li>
            <li><a href="/genres" class="nav-header"><i class="fa fa-fw fa-file"></i> Жанры</a></li>
            <li><a href="/organizers" class="nav-header"><i class="fa fa-fw fa-file"></i> Организаторы</a></li>
        </ul></li>
</ul>
