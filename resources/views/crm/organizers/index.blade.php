@extends('layouts.crm')

@section('content')
    <div class="btn-toolbar list-toolbar">

        <a href="/createorganizer" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
        <!--
        <button class="btn btn-default">Import</button>
        <button class="btn btn-default">Export</button>
        -->
        <div class="btn-group">
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Организатор</th>
            <th style="width: 3.5em;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($organizers as $organizer)
            <tr>
                <td>{{ $organizer->id }}</td>
                <td>{{ $organizer->organizer }}</td>
                <td>
                    <a href="/editorganizer/{{ $organizer->id }}"><i class="fa fa-pencil"></i></a>
                    <a href="/deleteorganizer/{{ $organizer->id }}"><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <!--
    <ul class="pagination">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
    -->
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Delete Confirmation</h3>
            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div>
          </div>
        </div>
    </div>
    -->
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Организаторы</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li class="active">Организаторы</li>
    </ul>
@endsection
