@extends('layouts.crm')

@section('content')


<div class="row">
    <div class="col-md-10">
        <br>
        <form id="tab" method="post" action="/saveuser">
            {{ csrf_field() }}
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">

                    <div class="form-group">
                        <label>Имя</label>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label>Пароль</label>
                        <input type="text" name="password" class="form-control" value="{{ old('password') }}">
                    </div>
                    <div class="form-group">
                        <label>Роли</label><br/>
                        <select name="role_id[]" class="form-control" id="roles"  multiple="multiple">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>

            <div class="btn-toolbar list-toolbar">
                <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>

            </div>
        </form>
    </div>
</div>
<!--
<div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
      </div>
      <div class="modal-body">

        <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-danger" data-dismiss="modal">Delete</button>
      </div>
    </div>
  </div>
</div>
-->
<script type="text/javascript">

    $(document).ready(function() {
        $('#roles').multiselect();
    });


</script>
@endsection

@section('header')
<!--
<div class="stats">
<p class="stat"><span class="label label-info">5</span> Tickets</p>
<p class="stat"><span class="label label-success">27</span> Tasks</p>
<p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

<h1 class="page-title">Новый пользователь</h1>
<ul class="breadcrumb">
    <li><a href="/">TEO CRM</a> </li>
    <li><a href="/users">Пользователи</a> </li>
    <li class="active">Новый пользователь</li>
</ul>
@endsection
