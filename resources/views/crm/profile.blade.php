@extends('layouts.crm')

@section('content')
<ul class="nav nav-tabs">
  <li class="active"><a href="#home" data-toggle="tab">Профиль</a></li>
  <li><a href="#profile" data-toggle="tab">Пароль</a></li>
</ul>

<div class="row">
  <div class="col-md-4">
    <br>
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
          <form id="tab" method="post" action="/updateprofile">
              {{ csrf_field() }}
        <div class="form-group">
        <label>Имя</label>
        <input type="text" value="{{ Auth::user()->name }}" class="form-control" name="name">
        </div>
        
        <div class="form-group">
        <label>Email</label>
        <input type="text" value="{{ Auth::user()->email }}" class="form-control" name="email">
        </div>
        <div>
              <button class="btn btn-primary">Обновить</button>
          </div>
          </form>
     </div>   

      <div class="tab-pane fade" id="profile">

        <form id="tab2"  method="post" action="/updatepassword">
            {{ csrf_field() }}
          <div class="form-group">
            <label>Новый пароль</label>
            <input type="password" class="form-control" name="password">
          </div>
          <div>
              <button class="btn btn-primary">Обновить</button>
          </div>
        </form>
      </div>
    

    
  </div>
</div>
</div>
@endsection

@section('header')
<!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

            <h1 class="page-title">Профиль</h1>
                    <ul class="breadcrumb">
            <li><a href="/">Главная</a> </li>
            <li class="active">Профиль</li>
        </ul>
@endsection
