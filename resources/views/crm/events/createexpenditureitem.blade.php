@extends('layouts.crm')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <br>
            <form id="tab" method="post" action="/saveexpenditureitem/{{ $event->id }}">
                {{ csrf_field() }}
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="home">
                        <div class="form-group">
                            <label>Группа</label><br/>
                            <select name="ccategory_id" class="form-control" id="buildings">
                                @foreach($ccategories as $ccat)
                                    <option value="{{ $ccat->id }}" >{{ $ccat->name }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Статья расходов</label>
                            <input type="text" name="name" class="form-control" value="{{ old('summ_cash ') }}">
                        </div>
                        <div class="form-group">
                            <label>Сумма нал</label>
                            <input type="text" name="summ_cash" class="form-control" value="{{ old('summ_cash ') }}">
                        </div>
                        <div class="form-group">
                            <label>Сумма безнал</label>
                            <input type="text" name="summ_nocash" class="form-control"  value="{{old('summ_nocash')}}">
                        </div>
                        <div class="form-group">
                            <label>Оплачено нал</label>
                            <input type="text" name="paid_cash" class="form-control" value="{{ old('paid_cash') }}">
                        </div>
                        <div class="form-group">
                            <label>Оплачено безнал</label>
                            <input type="text" name="paid_nocash" class="form-control" value="{{ old('paid_nocash') }}">
                        </div>
                    </div>


                </div>

                <div class="btn-toolbar list-toolbar">
                    <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>

                </div>
            </form>
        </div>
    </div>
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Delete Confirmation</h3>
          </div>
          <div class="modal-body">

            <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?</p>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-danger" data-dismiss="modal">Delete</button>
          </div>
        </div>
      </div>
    </div>
    -->
    <script type="text/javascript">

    </script>
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Новая сатья расходов</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li class="active">Новая статья расходов</li>
    </ul>
@endsection
