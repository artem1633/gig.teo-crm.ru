@extends('layouts.crm')

@section('content')
    <div class="btn-toolbar list-toolbar">

        <a href="/createevent" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
        <!--
        <button class="btn btn-default">Import</button>
        <button class="btn btn-default">Export</button>
        -->
        <div class="btn-group">
        </div>
    </div>
    <div class="btn-group">
        <label for="sel1">Фильтр</label>
        <select class="form-control" id="calEventFilter">
            <option value="all">все концерты</option>
            <option value="1">наши концерты</option>
            <option value="0">концерты конкурентов</option>
        </select>
    </div>
    <div class="row">
        <div class="col-md-10" id="calendar"></div>
    </div>

    <!--
    <ul class="pagination">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
    -->
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Delete Confirmation</h3>
            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div>
          </div>
        </div>
    </div>
    -->
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Концерты</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li class="active">Концерты</li>
    </ul>

<script>
   
    $(function() {
    $("#calendar").fullCalendar({
            eventSources: [{
                url: '/ajax/getevents/all',
                type: 'GET',
                data: {
                    op: 'source',
                },
                error: function() {
                    alert('Ошибка соединения с источником данных!');
                }
            }],
            displayEventTime: false,
            firstDay: 1,
            header: {
                left: '',
                center: 'title'
            },
            monthNames: ['Январь','Февраль','Март','Апрель','Май','οюнь','οюль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','οюнь','οюль','Авг.','Сент.','Окт.','Ноя.','Дек.'],
            dayNames: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
            dayNamesShort: ["ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"],
            eventClick: function(calEvent, jsEvent, view) {
                location.href ='/event/'+calEvent.id;
            },
            eventRender: function (eventObj, $el) {
                if(eventObj.itemStatus == 'new-event'){
                    $el.css({
                        'background-color': '#42d038',
                    });
                }
                if(eventObj.eventOrganisation == 'Конкурент'){
                    $el.css({
                        'border': '2px solid #e4c116',
                    });
                }

            }
        });
    });

    $(document).ready(function(){
        $('#calEventFilter').change(function(){
            var term = $(this).find(":selected").val();
            var source = '/ajax/getevents/' + term;
            $('#calendar').fullCalendar('removeEventSources');
            $('#calendar').fullCalendar( 'addEventSource', source );
        });
    });
</script>
@endsection
