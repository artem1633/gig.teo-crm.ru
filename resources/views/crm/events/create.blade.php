@extends('layouts.crm')

@section('content')
    <div class="row">
        <div class="col-md-10">
            <br>
            <form id="tab" method="post" enctype="multipart/form-data" action="/saveevent">
                {{ csrf_field() }}
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="home">
                        <div class="form-group" id="cityFormGroup">
                            <label>Выберите город или создать новое ниже</label>
                            <select name="city_id" id="city" class="form-control"  onchange="loadPlatforms(this, '{{$citiesLastInsertedId + 1}}', '{{$platformLastInsertedId + 1}}')">

                                <option value="0" @if(old('city_id') == 0 ) selected @endif ></option>
                                <option value="newCity">Создайте Город</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" >{{ $city->city }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group" id="platformFormGroup">
                            <label>Площадка</label>
                            <select name="platform_id" id="platform" class="form-control"  onchange="addNewPlatform('{{$platformLastInsertedId + 1}}')" disabled="disabled">
                                <option value="0">Выберите Площадка</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Жанр</label><br/>
                            <select name="genre_id" class="form-control" id="buildings">
                                @foreach($genres as $genre)
                                    <option value="{{ $genre->id }}" @if($genre->id == old('genre_id')) selected @endif >{{ $genre->genre }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Организатор</label><br/>
                            <select name="organizer_id" class="form-control" id="rooms">
                                @foreach($organizers as $organizer)
                                    <option value="{{ $organizer->id }}" @if($organizer->id == old('organizer_id')) selected @endif >{{ $organizer->organizer }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name ') }}">
                        </div>
                        <div class="form-group">
                            <label>Дата</label>
                            <input type="text" name="data_time" class="form-control  form_datetime"  readonly value="{{old('datatime')}}">
                        </div>
                        <div class="form-group">
                            <label>Число билетов</label>
                            <input type="text" name="tickets_count" class="form-control" value="{{ old('ticket_count') }}">
                        </div>
                        <div class="form-group">
                            <label>Цена билета</label>
                            <input type="text" name="ticket_price" class="form-control" value="{{ old('ticket_price') }}">
                        </div>
                        <div class="form-group">
                            <label>Вал</label>
                            <input type="text" name="summ" class="form-control" value="{{ old('summ') }}">
                        </div>
                        <div class="form-group">
                            <label>Организатор</label>
                            <div>
                                <label class="radio-inline">
                                  <input type="radio" name="is_our" value="1" checked="checked"> Наша компания 
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_our" value="0"> Конкурент
                                </label>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="btn-toolbar list-toolbar">
                    <button class="btn btn-primary"><i class="fa fa-save"></i> Сохранить</button>

                </div>
            </form>
        </div>
    </div>
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Delete Confirmation</h3>
          </div>
          <div class="modal-body">

            <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?</p>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-danger" data-dismiss="modal">Delete</button>
          </div>
        </div>
      </div>
    </div>
    -->
    <script type="text/javascript">

        function loadPlatforms(select, cid, platformId) {
            var platformSelect = $('select[name="platform_id"]');
            var city = document.getElementById("city");
            var platform = document.getElementById("platform");
            var cityFormGroup = $('#cityFormGroup');
            var platformFormGroup = $('#platformFormGroup');
            $.getJSON('/ajax/getplatforms', { city:select.value}, function(platformList){
                platformSelect.html(''); // очищаем список
                platformSelect.append('<option></option>');
                platformSelect.append('<option value="newPlatform">Создайте Площадка</option>');
                // заполняем списо новыми пришедшими данными
                $.each(platformList, function(i){
                    platformSelect.append('<option value="' + this.id + '">' + this.platform + '</option>');
                });
                platformSelect.removeAttr('disabled');
            });

            var selectedCityValue = city.options[city.selectedIndex].value;
            if(selectedCityValue === 'newCity'){
                $('#newPlatformForm2').remove();
                newCityForm = '<div id="newCityForm"><br><label style=" color: #22ad47;">Введите новое название города</label>';
                newCityForm += '<input type="text" name="city" class="form-control" value="{{ old('city') }}"><input type="hidden" name="city_id" class="form-control" value="'+cid+'"></div>';

                cityFormGroup.append(newCityForm);

                platformSelect.prop('disabled', 'disabled');

                newPlatformForm = '<div id="newPlatformForm"><br><label style=" color: #22ad47;">Введите новое Площадка</label>';
                newPlatformForm += '<input type="text" name="platform" class="form-control" value="{{ old('platform') }}"><input type="hidden" name="platform_id" class="form-control" value="'+platformId+'"></div>';
                platformFormGroup.append(newPlatformForm);


            }
            else{
                $('#newCityForm').remove();
                $('#newPlatformForm').remove();
                $('#newPlatformForm2').remove();
            }

        }

        function addNewPlatform(platformId){
            var platform2 = document.getElementById("platform");
            var platformFormGroup2 = $('#platformFormGroup');
            var selectedPlatformValue = platform2.options[platform2.selectedIndex].value;
            if(selectedPlatformValue === 'newPlatform'){
                 newPlatformForm2 = '<div id="newPlatformForm2"><br><label style=" color: #22ad47;">Введите новое Площадка</label>';
                newPlatformForm2 += '<input type="text" name="platform" class="form-control" value="{{ old('platform2') }}"><input type="hidden" name="platform_id" class="form-control" value="'+platformId+'"></div>';
                platformFormGroup2.append(newPlatformForm2);
            }else{
                $('#newPlatformForm2').remove();
            }
        }
        
    </script>
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Новый концерт</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li class="active">Новый концерт</li>
    </ul>
@endsection
