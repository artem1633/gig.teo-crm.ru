@extends('layouts.crm')

@section('content')
    <h3>{{ $event->name }}</h3>
    <p>{{ date('Y-m-d j:m', strtotime($event->happened_at)) }}</p>
    <p>{{ $event->genre->genre }}</p>
    <p>{{ $event->city->city }} - {{ $event->platform->platform }}</p>
    <p>Организатор: {{ $event->organizer->organizer }}</p>
    <div class="btn-toolbar list-toolbar">
<!--
        <a href="/creatgenre" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>

        <a href="/event/{{ $event->id }}/sales"button class="btn btn-default">Продажи</a>
        -->
        <a href="/event/{{ $event->id }}/costs" class="btn btn-default">Расходы</a>
        <a href="/event/{{ $event->id }}/sales" class="btn btn-default">Продажи</a>
        <div class="btn-group">
        </div>
    </div>

@endsection

@section('header')
    <!--
<div class="stats">
<p class="stat"><span class="label label-info">5</span> Tickets</p>
<p class="stat"><span class="label label-success">27</span> Tasks</p>
<p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">{{ $event->name }}</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li><a href="/">Концерты</a> </li>
        <li class="active">{{ $event->name }}</li>
    </ul>
@endsection
