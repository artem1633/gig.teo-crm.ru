@extends('layouts.crm')

@section('content')
    <form method="post" action="/event/{{$event->id}}/sales">
        @csrf
    <div class="btn-toolbar list-toolbar">

        <!--
                        <button class="btn btn-default">Import</button>
                        <button class="btn btn-default">Export</button>
                        -->
        <div class="btn-group">
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>Продано билето</th>
            <th>Сумма прода</th>
            <th>Дата продажи</th>
        </tr>
        </thead>
        <tbody>
	        <tr>
	            <td>
            		<div class="form-group">
		                <div class='input-group'>
							<input type="number" name="qty" value="" class="form-control"/>
		                </div>
		            </div>
	            </td>
	            <td>
	            	<div class="form-group">
		                <div class='input-group'>
		                    <input type="number" name="price" value="" class="form-control" />
		                </div>
		            </div>
            	</td>
	            <td>
	            	<div class="form-group">
		                <div class='input-group date'>
		                    <input type='text' name="sale_at" class="form-control form_date" autocomplete="off" />
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
		            </div>
	            </td>
	            <td>
	            	<button type="submit" class="btn btn-primary"> Добавить</button>
	            </td>
	        </tr>
        </tbody>
    </table>

    <div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">Продажи </div>

	  <!-- Table -->
	  <table class="table table-striped table-bordered">
	    <thead> 
	    	<tr>  
	    		<th>Сумма продаж</th> 
	    		<th>Продано билетов</th> 
	    		<th>Дата продажи</th> 
	    	</tr> 
	    </thead>
	    <tbody>
	    	@foreach($event->sales as $sale)
	            <tr>
	                <td>{{ $sale->price }}</td>
	                <td>{{ $sale->qty }}</td>
	                <td>{{ $sale->sale_at }}</td>
               </tr>
	        @endforeach
	        <tr style="background-color: #cacaca">
           		<td><strong>Итоговая сумма | {{$event->sales->sum('price')}} .руб</strong></td>
           		<td><strong>Всего билетов | {{$event->sales->sum('qty')}} .руб</strong></td>
           		<td></td>
           </tr>
	    </tbody>
	  </table>
	</div>
    </form>
    <!--
    <ul class="pagination">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
    -->
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Delete Confirmation</h3>
            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div>
          </div>
        </div>
    </div>
    -->
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Продажи | {{ $event->name }}</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li><a href="/event/{{$event->id}}">{{ $event->name }}</a></li>
        <li class="active">Продажи</li>

    </ul>
@endsection
