@extends('layouts.crm')

@section('content')
    <div class="btn-toolbar list-toolbar">

        @if(Auth::user()->hasAnyRole(['Admin', 'Manager']))
            <a href="/createexpenditureitem/{{ $event->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить статью расходов</a> 
        @endif 
        <!--
        <button class="btn btn-default">Import</button>
        <button class="btn btn-default">Export</button>
        -->
        <div class="btn-group">
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>Статья расходов</th>
            <th>Сумма нал</th>
            <th>Сумма безнал</th>
            <th>Опл нал</th>
            <th>Опл безнал</th>
            <th>Итого нал</th>
            <th>Итого безнал</th>
            <th style="width: 3.5em;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($ccategories as $ccateg)
            <tr>
                <td><strong>{{ $ccateg->name }}</strong></td>
                <td>{{ $ccateg->expenditureitems->sum('summ_cash') }}</td>
                <td>{{ $ccateg->expenditureitems->sum('summ_nocash') }}</td>
                <td>{{ $ccateg->expenditureitems->sum('paid_cash') }}</td>
                <td>{{ $ccateg->expenditureitems->sum('paid_nocash') }}</td>
                <td>{{ $ccateg->expenditureitems->sum('summ_cash') - $ccateg->expenditureitems->sum('paid_cash') }}</td>
                <td>{{ $ccateg->expenditureitems->sum('summ_nocash') - $ccateg->expenditureitems->sum('paid_nocash') }}</td>
                <td>
                    <!--
                    <a href="/editcity/{{ $ccateg->id }}"><i class="fa fa-pencil"></i></a>
                    <a href="/deletesity/{{ $ccateg->id }}"><i class="fa fa-trash-o"></i></a>
                    -->
                </td>
            </tr>
            @foreach($ccateg->expenditureitems as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->summ_cash }}</td>
                    <td>{{ $item->summ_nocash }}</td>
                    <td>{{ $item->paid_cash }}</td>
                    <td>{{ $item->paid_nocash }}</td>
                    <td>{{ $item->summ_cash - $item->paid_cash }}</td>
                    <td>{{ $item->summ_nocash - $item->paid_nocash }}</td>
                    <td>
                    <a href="/editexpenditureitem/{{ $item->id }}"><i class="fa fa-pencil"></i></a>
                    <a href="/deleteexpenditureitem/{{ $item->id }}"><i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            @endforeach
        @endforeach
        <tr style="background-color: #cacaca">
            <td></td>
            <td><strong>{{ $event->expenditureitems->sum('summ_cash') }}</strong></td>
            <td><strong>{{ $event->expenditureitems->sum('summ_nocash') }}</strong></td>
            <td><strong>{{ $event->expenditureitems->sum('paid_cash') }}</strong></td>
            <td><strong>{{ $event->expenditureitems->sum('paid_nocash') }}</strong></td>
            <td><strong>{{ $event->expenditureitems->sum('summ_cash')  - $event->expenditureitems->sum('paid_cash')  }}</strong></td>
            <td><strong>{{ $event->expenditureitems->sum('summ_nocash') - $event->expenditureitems->sum('paid_nocash') }}</strong></td>
            <td>
            </td>
        </tr>
        </tbody>
    </table>
    <!--
    <ul class="pagination">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
    -->
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Delete Confirmation</h3>
            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div>
          </div>
        </div>
    </div>
    -->
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Расходы</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li><a href="/event/{{ $event->id }}">{{ $event->name }}</a> </li>
        <li class="active">Расходы</li>
    </ul>
@endsection
