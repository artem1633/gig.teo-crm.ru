@extends('layouts.crm')

@section('content')
    <form method="post" action="/updatesales">
        @csrf
    <div class="btn-toolbar list-toolbar">

                <!-- <button type="submit" class="btn btn-primary"> Сохранить</button> -->
        <!--
                        <button class="btn btn-default">Import</button>
                        <button class="btn btn-default">Export</button>
                        -->
        <div class="btn-group">
        </div>
    </div>

    <table class="table table-striped table-bordered" id="dtListEvents">
        <thead>
        <tr>
            <th></th>
            <th>Концерт</th>
            <th>Продано билетов</th>
            <th>Сумма продаж</th>
            <th>Организатор</th>
            <th>Дата</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </form>
    <style>
        .details-control{
            padding: 0 15px !important;
        }
        td.details-control{
            background: url(/img/details_open.png) no-repeat center center;
            cursor: pointer;
        }
        tr.details td.details-control {
            background: url(/img/details_close.png) no-repeat center center;
        }
    </style>
    <script>
        function format ( d ) {
            var details = '';
            details += '<table class="table table-striped table-bordered">';
            details += '<thead><tr>';
            details += '<th>Сумма продаж</th>';
            details += '<th>Продано билетов</th>';
            details += '<th>Дата продажи</th>';
            details += '</tr></thead>';
            $.each(d.sales, function( index, value ) {
                details += '<tr><td>'+value.price+'</td><td>'+value.qty+'</td><td>'+value.sale_at+'</td></tr>';
            });
            details += '</trable>';
            return details;
        }

        $(document).ready(function(){
            var dtListEvents = $("#dtListEvents").DataTable({
                "bLengthChange": true,
                "bPaginate": true,
                "bInfo": true,
                "autoWidth": false,  
                "order": [[0, "desc"]],
                "ajax": {
                    "processing": true,
                    "serverSide": true,
                    "url": "/ajax/getevents/all",
                    "dataSrc":"",
                    "type": "GET"
                },
                "columns": [
                    {
                        "class":          "details-control",
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ""
                    },
                    { "data": "title",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/event/"+oData.id+"'>"+oData.title+"</a>");
                        }
                    },
                    { "data": "totalSoldPrice" },
                    { "data": "totalSoldTickets" },
                    { "data": "eventOrganisation" },
                    { "data": "end" },
                    

                ],
                "order": [[1, 'asc']]
            });

            // Array to track the ids of the details displayed rows
            var detailRows = [];
         
            $('#dtListEvents tbody').on( 'click', 'tr td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = dtListEvents.row( tr );
                var idx = $.inArray( tr.attr('id'), detailRows );
         
                if ( row.child.isShown() ) {
                    tr.removeClass( 'details' );
                    row.child.hide();
         
                    // Remove from the 'open' array
                    detailRows.splice( idx, 1 );
                }
                else {
                    tr.addClass( 'details' );
                    row.child( format( row.data() ) ).show();
         
                    // Add to the 'open' array
                    if ( idx === -1 ) {
                        detailRows.push( tr.attr('id') );
                    }
                }
            } );
         
            // On each draw, loop over the `detailRows` array and show any child rows
            dtListEvents.on( 'draw', function () {
                $.each( detailRows, function ( i, id ) {
                    $('#'+id+' td.details-control').trigger( 'click' );
                } );
            } );

            // dtListEvents.ajax.url('/ajax/getevents/all').load();

            // $('#statusFilter').on('change', function(){
            //   var filter_value = $(this).val();
            //   var new_url = '/ajax/getevents/'+filter_value;
            //   dtListEvents.ajax.url(new_url).load();
            // });
        });
    </script>
    <!--
    <ul class="pagination">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
    -->
    <!--
    <div class="modal small fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Delete Confirmation</h3>
            </div>
            <div class="modal-body">
                <p class="error-text"><i class="fa fa-warning modal-icon"></i>Are you sure you want to delete the user?<br>This cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <button class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div>
          </div>
        </div>
    </div>
    -->
@endsection

@section('header')
    <!--
<div class="stats">
    <p class="stat"><span class="label label-info">5</span> Tickets</p>
    <p class="stat"><span class="label label-success">27</span> Tasks</p>
    <p class="stat"><span class="label label-danger">15</span> Overdue</p>
</div>
-->

    <h1 class="page-title">Продажи</h1>
    <ul class="breadcrumb">
        <li><a href="/">TEO CRM</a> </li>
        <li class="active">Продажи</li>
    </ul>
@endsection
