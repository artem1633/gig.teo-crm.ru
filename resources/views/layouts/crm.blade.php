<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>TEO CRM</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/assets/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/assets/lib/font-awesome/css/font-awesome.css">
    <link href="/assets/pickmeup/css/pickmeup.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link href="/assets/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>

        <script src="/assets/lib/jQuery-Knob/js/jquery.knob.js" type="text/javascript"></script>
        <!-- DataTables JavaScript -->
    <script src="/assets/datatables/media/js/jquery.dataTables.js"></script>
    <script src="/assets/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $(".knob").knob();
        });
    </script>


    <link rel="stylesheet" type="text/css" href="/assets/stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="/assets/stylesheets/premium.css">
    
    <!-- include summernote -->
    <link rel="stylesheet" href="/assets/lib/summernote/summernote.css">
    <script type="text/javascript" src="/assets/lib/summernote/summernote.js"></script>
    <script type="text/javascript" src="/assets/lib/summernote/summernote-ru-RU.js"></script>
    <script src="/assets/pickmeup/js/jquery.pickmeup.min.js"></script>
    <script type="text/javascript" src="/assets/lib/bsmultiselect/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="/assets/lib/bsmultiselect/css/bootstrap-multiselect.css" type="text/css"/>
    <link rel="stylesheet" href="/assets/datatimepicker/css/bootstrap-datetimepicker.css" type="text/css"/>
    <script src="/assets/datatimepicker/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="/assets/datatimepicker/js/locales/bootstrap-datetimepicker.ru.js" type="text/javascript"></script>
    <script type='text/javascript' src='/assets/fullcalendar/moment.min.js'></script>
    <link rel='stylesheet' type='text/css' href='/assets/fullcalendar/fullcalendar.css' />
    <link rel='stylesheet' type='text/css' href='/assets/fullcalendar/fullcalendar.print.css' media='print' />
    <script type='text/javascript' src='/assets/fullcalendar/fullcalendar.js'></script>

    <script type="text/javascript">
    $(function() {
        
        $(".form_datetime").datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            language:  'ru',
            autoclose: true
        });

        $(".form_date").datetimepicker({
            format: 'yyyy-mm-dd',
            language:  'ru',
            minView: 2,
            pickTime: false,
            autoclose: true
        });

      $('.summernote').summernote({
        height: 200,
        tabsize: 2,
        lang: 'ru-RU',
        onImageUpload: function(files,editor,welEditable){
            console.log('image upload:', files, editor, welEditable);
            sendFile(files[0],editor,welEditable);
        }
      });
      
      
    function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "/upload-editor-image",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
      
    });

    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true ,
                language: {
                    url: "/assets/datatables-plugins/i18n/Russian.lang"
                },
                "order": [[ 0, "asc" ]]
        });
        $('#dataTables-bids').DataTable({
                responsive: true ,
                language: {
                    url: "/assets/datatables-plugins/i18n/Russian.lang"
                },
                "order": [[ 0, "desc" ]]
        });
        $(".form_datetime").pickmeup({
            format: 'd-m-Y'
        });
        
    });  
    
  </script>

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="/cpanel/dashboard"><span class="navbar-brand"><span class="fa fa-paper-plane"></span> TEO CRM</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> {{ Auth::user()->name }}
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="/profile">Мой аккаунт</a></li>
                <li class="divider"></li>
                <!--
                <li class="dropdown-header">Admin Panel</li>
                <li><a href="/cpanel/users">Пользователи</a></li>
                <li><a href="./">Security</a></li>
                <li><a tabindex="-1" href="./">Payments</a></li>
                <li class="divider"></li>
                -->
                <li>
                    <a tabindex="-1" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Выйти
                                        </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                </li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    

    <div class="sidebar-nav">
    @include('crm.lmenu')
    </div>

    <div class="content">
        <div class="header">
            
@yield('header')

        </div>
        <div class="main-content">

@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
@if ($message = Session::get('success'))
                          <div class="alert alert-success">
                            <p>
                              {{ $message }}
                            </p>
                          </div>
                        @endif
                        @if ($message = Session::get('warning'))
                          <div class="alert alert-warning">
                            <p>
                              {{ $message }}
                            </p>
                          </div>
                        @endif
@yield('content')

            <footer>
                <hr>


                <p>Copyright© 2018 <a href="javascript:void(0)" target="_blank">TEO.</a> All right reserved</p>
            </footer>
        </div>
    </div>


    <script src="/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
